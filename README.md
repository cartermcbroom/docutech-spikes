
## POCs for docutech

**Get SSO AWS credentials**
	   
	# configure a new profile: instil-dev is the name of the profile i created 
    aws sso configure --profile instil-dev
    aws sso login --profile instil-dev && yawsso
    
    # optional - set the default profile for aws cli 
    export AWS_PROFILE=profile_name
    

## Terraform setup

**Install terraform**

    brew tap hashicorp/tap
    brew install hashicorp/tap/terraform
    brew update

**Terraform commands**

 - terraform init - prepares working directory for running other commands. 
- terraform fmt - formats files 
- terraform validate - checks whether the configuration is valid
- terraform plan - outputs the changes required by the current configuration (append -out to save)
- terraform apply - create or update the infrastructure using the current configuration
- terraform destroy - destroy the previously created infrastructure 