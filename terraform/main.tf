terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}

provider "aws" {
  region  = "eu-west-1"
  profile = "instil-dev"
}

# s3 bucket config to trigger lambda
resource "aws_s3_bucket" "input_bucket" {
  bucket = "email-input-bucket"
}

resource "aws_lambda_permission" "allow_s3_trigger_lambda" {
  statement_id  = "s3Invoke"
  depends_on    = [module.parser]
  action        = "lambda:InvokeFunction"
  function_name = module.parser.parser_function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.input_bucket.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "email-input-bucket"
  lambda_function {
    lambda_function_arn = module.parser.parser_function_arn
    events              = ["s3:ObjectCreated:*"]
  }
}

# Parser lambda
module "parser" {
  source               = "./parser-function"
  parser_function_file = "parser.zip"
  email_topic_arn      = aws_sns_topic.send_email_sns.arn
  depends_on           = [aws_sns_topic.send_email_sns]
}

# Email sending lambda
module "email_sender" {
  source                     = "./email-sender-function"
  email_sender_function_file = "emailSender.zip"
}

resource "aws_lambda_event_source_mapping" "email_event_source_mapping" {
  enabled          = true
  batch_size       = 1
  event_source_arn = aws_sqs_queue.send_email_queue.arn
  function_name    = module.email_sender.email_sender_function_arn
}

# SQS and SNS config
resource "aws_sns_topic" "send_email_sns" {
  name = "send-email"
}

resource "aws_sqs_queue" "send_email_queue" {
  name                       = "send-email-queue"
  visibility_timeout_seconds = "40"
}

resource "aws_sqs_queue_policy" "email_sqs_queue_policy" {
  queue_url = aws_sqs_queue.send_email_queue.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement : [
      {
        Action : [
          "sqs:SendMessage"
        ],
        Effect : "Allow"
        Resource : [
          "arn:aws:sqs:*:*:send-email-queue"
        ]
        Principal : "*"
        Condition : {
          "ArnEquals" : {
            "aws:SourceArn" : "${aws_sns_topic.send_email_sns.arn}"
          }
        }
      }
    ]
  })
}

resource "aws_sns_topic_subscription" "send_email_sns_subscription" {
  topic_arn = aws_sns_topic.send_email_sns.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.send_email_queue.arn
}
