#  Terraform config for email sender lambda_action

resource "aws_lambda_function" "email_sender_function" {
  filename         = var.email_sender_function_file
  function_name    = "EmailSenderFunction"
  role             = aws_iam_role.email_lambda_role.arn
  handler          = "EmailSender::EmailSender.Function::FunctionHandler"
  source_code_hash = filebase64sha256("${var.email_sender_function_file}")
  timeout          = "40"
  depends_on = [
    aws_iam_role_policy_attachment.email_lambda_policy_attachment
  ]
  runtime = "dotnet6"
}

# Policies for email sender function
resource "aws_iam_policy" "email_lambda_policy" {
  name = "emailSenderFunctionPolicy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement : [
      {
        Action : [
          "logs:CreateLogStream",
          "logs:CreateLogGroup",
          "logs:PutLogEvents"
        ],
        Effect : "Allow"
        Resource : [
          "arn:aws:logs:*:*:*"
        ]
      },
      {
        Action : [
          "ses:SendEmail",
          "ses:SendRawEmail"
        ],
        Effect : "Allow"
        Resource : "*"
      },
      {
        Action : [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ],
        Effect : "Allow"
        Resource : "arn:aws:sqs:*:*:send-email-queue"
      }
    ]
  })
}

resource "aws_iam_role" "email_lambda_role" {
  name = "email_lambda_function_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "email_lambda_policy_attachment" {
  role       = aws_iam_role.email_lambda_role.id
  policy_arn = aws_iam_policy.email_lambda_policy.arn
}
