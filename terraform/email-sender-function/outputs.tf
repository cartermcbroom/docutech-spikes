output "email_sender_function_arn" {
  value = aws_lambda_function.email_sender_function.arn
}

output "email_function_name" {
  value = "EmailSenderFunction"
}
