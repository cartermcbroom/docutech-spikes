# Terraform config for parser Lambda

resource "aws_lambda_function" "parser_function" {
  filename         = var.parser_function_file
  function_name    = "ParserFunction"
  role             = aws_iam_role.lambda_role.arn
  handler          = "Parser::Parser.Function::FunctionHandler"
  source_code_hash = filebase64sha256("${var.parser_function_file}")
  timeout          = "40"
  depends_on = [
    aws_iam_role_policy_attachment.parser_lambda_policy_attachment
  ]
  runtime = "dotnet6"
  environment {
    variables = {
      EMAIL_TOPIC_ARN = "${var.email_topic_arn}"
    }
  }
}

# TODO - convert policy statements into policy document resource - better practice
#policies for parser function
resource "aws_iam_policy" "lambda_policy" {
  name = "parserFunctionPolicy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement : [
      {
        Action : [
          "s3:GetObject"
        ],
        Effect : "Allow"
        Resource : [
          "arn:aws:s3:::email-input-bucket/*",
          "arn:aws:s3:::email-input-bucket"
        ]
      },
      {
        Action : [
          "logs:CreateLogStream",
          "logs:CreateLogGroup",
          "logs:PutLogEvents"
        ],
        Effect : "Allow"
        Resource : [
          "arn:aws:logs:*:*:*"
        ]
      },
      {
        Action : [
          "sns:Publish"
        ],
        Effect : "Allow"
        Resource : [
          "arn:aws:sns:*:*:send-email"
        ]
      }
    ]
  })
}

resource "aws_iam_role" "lambda_role" {
  name = "parser_lambda_function_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "parser_lambda_policy_attachment" {
  role       = aws_iam_role.lambda_role.id
  policy_arn = aws_iam_policy.lambda_policy.arn
}
