output "parser_function_arn" {
  value = aws_lambda_function.parser_function.arn
}

output "parser_function_name" {
  value = "ParserFunction"
}
