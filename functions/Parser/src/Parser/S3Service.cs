using Amazon.Lambda.Core;
using Amazon.S3;
using Amazon.S3.Model;

namespace DefaultNamespace;

public class S3Service {
    private readonly AmazonS3Client _s3Client;

    public S3Service(AmazonS3Client s3Client) {
        _s3Client = s3Client;
    }

    public async Task<GetObjectResponse> GetS3Object(string bucketName, string objectKey) {
        LambdaLogger.Log($"Attempting to fetch object {objectKey} from bucket : {bucketName}");

        try {
            var s3GetResponse = await _s3Client.GetObjectAsync(CreateS3GetRequest(bucketName, objectKey));
            LambdaLogger.Log("Successfully retrieved object from S3.");
            return s3GetResponse;
        }
        catch (Exception) {
            LambdaLogger.Log("Failed to retrieve object from S3...");
            throw;
        }
    }

    private GetObjectRequest CreateS3GetRequest(string bucketName, string objectKey) {
        return new GetObjectRequest {
            BucketName = bucketName,
            Key = objectKey
        };
    }
}