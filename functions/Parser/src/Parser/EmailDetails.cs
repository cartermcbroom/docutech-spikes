using Newtonsoft.Json;

namespace DefaultNamespace;

public class EmailDetails
{
    [JsonProperty(PropertyName = "sender_email")]
    public string senderEmail { get; set; }
    
    [JsonProperty(PropertyName = "receiver_email")]
    public string receiverEmail { get; set; }
    
    [JsonProperty(PropertyName = "content")]
    public string content { get; set; }
}