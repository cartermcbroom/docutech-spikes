using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using DefaultNamespace;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Parser;

public class Function {
    private static readonly string TopicArn = Environment.GetEnvironmentVariable("EMAIL_TOPIC_ARN") ?? "";
    private static readonly AmazonSimpleNotificationServiceClient SnsClient = new();
    private static readonly AmazonS3Client S3Client = new(RegionEndpoint.EUWest1);
    private readonly S3Service _s3Service = new(S3Client);

    public async Task FunctionHandler(S3Event input, ILambdaContext context) {
        LambdaLogger.Log("Lambda triggered via S3 Event");

        var s3EventRecord = input.Records[0].S3;
        var bucketName = s3EventRecord.Bucket.Name;
        var objectKey = s3EventRecord.Object.Key;

        try {
            var s3Object = await _s3Service.GetS3Object(bucketName, objectKey);

            var snsMessage = GetEmailDetailsFromS3Object(s3Object);

            LambdaLogger.Log($"sending SNS message... to topic arn: {TopicArn}");

            var attributes = new Dictionary<string, MessageAttributeValue>
            {
                {"senderEmail", new MessageAttributeValue { DataType = "String", StringValue = snsMessage.senderEmail}},
                {"receiverEmail", new MessageAttributeValue { DataType = "String", StringValue = snsMessage.receiverEmail}},
                {"content", new MessageAttributeValue { DataType = "String", StringValue = snsMessage.content}}
            };

            var snsResponse = await SnsClient.PublishAsync(new PublishRequest {
                TopicArn = TopicArn,
                Message = "Email payload from SNS",
                MessageAttributes = attributes
            });

            LambdaLogger.Log($"SNS response HTTP code {snsResponse.HttpStatusCode}");
            LambdaLogger.Log("Message successfully published");
        }
        catch (Exception e) {
            LambdaLogger.Log($"Exception caught {e.Message}");
        }
    }

    private static EmailDetails GetEmailDetailsFromS3Object(GetObjectResponse s3Object) {
        try {
            LambdaLogger.Log("Attempting to read data from S3 object..");
            using var reader = new StreamReader(s3Object.ResponseStream);
            var contents = reader.ReadToEnd();
            LambdaLogger.Log($"Deserializing s3 object contents : {contents}");
            var emailDetails = JsonConvert.DeserializeObject<EmailDetails>(contents);
            reader.Close();

            if (emailDetails != null) return emailDetails;
            throw new Exception("Failed to deserialize email details from S3 object");
        }
        catch (Exception e) {
            throw new Exception(
                $"Exception caught validating Json Email data in S3 Object, message : {e.Message}");
        }
    }
}