using Amazon.Lambda.Core;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;

namespace DefaultNamespace;

public class SesService {
    private readonly AmazonSimpleEmailServiceClient _sesClient;
    
    public SesService(AmazonSimpleEmailServiceClient sesClient) {
        _sesClient = sesClient;
    }

    public async Task<SendEmailResponse> SendEmail(string senderEmail, string receiverEmail, string subject, string content) {
        LambdaLogger.Log("Attempting to send email...");
        try {
            var emailRequest = CreateEmailRequest(senderEmail, receiverEmail, subject, content);
            var emailResponse = await _sesClient.SendEmailAsync(emailRequest);
            LambdaLogger.Log("Email sent successfully..");
            return emailResponse;
        }
        catch (Exception e) {
            throw new Exception($"Exception caught trying to send email: {e.Message}");
        }
    }
    
    private SendEmailRequest CreateEmailRequest(string senderEmail, string receiverEmail, string subject, string content) {
        return new SendEmailRequest {
            Source = senderEmail,
            Destination = new Destination {
                ToAddresses = new List<string> {receiverEmail}
            },
            Message = new Message {
                Subject = new Content(subject),
                Body = new Body {
                    Html = new Content {
                        Charset = "UTF-8",
                        Data = content
                    }
                }
            }
        };
    }
}