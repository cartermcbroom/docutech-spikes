using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.SNSEvents;
using Amazon.Lambda.SQSEvents;
using Amazon.SimpleEmail;
using DefaultNamespace;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace EmailSender;

public class Function {
    private static readonly AmazonSimpleEmailServiceClient SesClient = new(RegionEndpoint.EUWest1);
    private readonly SesService _sesService = new(SesClient);
    
    public async Task FunctionHandler(SQSEvent input, ILambdaContext context) {
        LambdaLogger.Log("SQS event triggered email lambda");
        var sqsMessageBody = input.Records[0].Body;
        
        try {
            var snsMessage = JsonConvert.DeserializeObject<SNSEvent.SNSMessage>(sqsMessageBody);
        
            LambdaLogger.Log("parsed SNS event : " + JsonConvert.SerializeObject(snsMessage));

            var senderEmail = snsMessage?.MessageAttributes["senderEmail"].Value ?? "sender not set";
            var receiverEmail = snsMessage?.MessageAttributes["receiverEmail"].Value ?? "receiver not set";
            var content = snsMessage?.MessageAttributes["content"].Value ?? "content not set";
            
            var response = await _sesService.SendEmail(senderEmail, receiverEmail, "FROM SES", content);
            
            LambdaLogger.Log("Email sent successfully");
            LambdaLogger.Log($"SES response HTTP code {response.HttpStatusCode}");
        }
        catch (Exception e) {
            LambdaLogger.Log("Failed to send email...");
            LambdaLogger.Log($"Exception caught: {e.Message}");
        }
    }
}
